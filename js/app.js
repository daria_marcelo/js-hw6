// 1) можливість записувати спеціальні символи як звичайні за допомогою додавання зворотного слеша перед спец.символом.
// 2) Три способи : Function Declaration, Function Expression и Named Function Expression.
// 3) Hoisting дозволяє нам використовувати функції перед їх оголошенням у коді.

function createNewUser() {

    const newUser = {
        getLogin: function () {
            return `${(this.firstName[0] || '').toLowerCase()}${this.lastName.toLowerCase()}`;
        },

        setFirstName: function (firstName) {
            Object.defineProperty(this, 'firstName', { writable: true, enumerable: true, configurable: true });
            this.firstName = firstName;
            Object.defineProperty(this, 'firstName', { writable: false, enumerable: true, configurable: true });
        },

        setLastName: function (lastName) {
            Object.defineProperty(this, 'lastName', { writable: true, enumerable: true, configurable: true });
            this.lastName = lastName;
            Object.defineProperty(this, 'lastName', { writable: false, enumerable: true, configurable: true });
        },

        birthday: null,

        getBirthday: function () {
            let date = prompt(`Enter your birth date dd.mm.yyyy`);
            while (date === null){
                date = prompt(`Enter your birth date dd.mm.yyyy`);
            }
            let splitDate = date.split(`.`);
            while (
                date === null ||
                    splitDate.length !== 3 ||
                    splitDate[0].length !== 2 ||
                    splitDate[1].length !== 2 ||
                    splitDate[2].length !== 4
                ){
                date = prompt(`Enter your birth date dd.mm.yyyy`);
                splitDate = date === null ? [] : date.split(`.`);
            }
            this.birthday = new Date(Number(splitDate[2]), Number(splitDate[1] - 1), Number(splitDate[0]));
        },

        getAge: function calcAge() {
            let n = new Date(),
                b = new Date(this.birthday),
                age = n.getFullYear() - b.getFullYear()
                return n.setFullYear(1970) < b.setFullYear(1970) ? age - 1 : age;

        },


    getPassword: function () {
        return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.getFullYear()}`
    }
    };
newUser.getBirthday();
    Object.defineProperties(newUser, {
        firstName: {
            value: prompt(`Enter your name`) || '',
            writable: false,
            configurable: true,
            enumerable: true,
        },
        lastName: {
            value: prompt(`Enter your last name`) || '',
            writable: false,
            configurable: true,
            enumerable: true,
        },
    });


    return newUser;
}


const newUser = createNewUser();
console.log(newUser.getPassword());
console.log(newUser.getAge());

